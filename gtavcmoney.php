<?php
	/*
	 * Written by xPaw
	 *
	 * Refference used: http://www.gtaforums.com/index.php?showtopic=457357
	 */
	
	define( 'BR', '<br>' );
	
	$WANTED_MONEY = (int)1000000;
	
	$Buffer = File_Get_Contents( "GTAVCsf1.b" );
	
	$CRC           = _UnPack( 'L', SubStr( $Buffer, -4 ) );
	$Buffer        = SubStr( $Buffer, 0, -4 );
	$CalculatedCRC = GTACheckSum( $Buffer );
	
	$Log  = "Savegame checksum: " . $CRC . BR;
	$Log .= "Calculated checksum: " . $CalculatedCRC . BR . BR;
	
	if( $CRC != $CalculatedCRC )
	{
		$Log .= "Different checksums, corrupt savegame?";
		goto End;
	}
	
	$BlockID = 0;
	$Pos = 0;
	
	/*
	 * Money is located in block #18, which is player info block
	 * We just go through all blocks and skipping them
	 *
	 * Then, in block #18,
	 * first 4 bytes are 'money'
	 * then 4 bytes after 15th byte is 'display money'
	 *
	 * We simply replace those bytes to our own money
	 * and update checksum at the end of file
	 */
	
	while( ++$BlockID < 19 )
	{
		$Pos += _UnPack( 'L', SubStr( $Buffer, $Pos, 4 ) ) + 4;
	}
	
	$Pos += 8; // Some crappy header, no idea
	
	$Money        = _UnPack( 'L', SubStr( $Buffer, $Pos, 4 ) );
	$MoneyDisplay = _UnPack( 'L', SubStr( $Buffer, $Pos + 15, 4 ) );
	
	$Log .= "Your current money: " . $Money . BR;
	
	if( $Money == $WANTED_MONEY )
	{
		$Log .= BR . "Your money is exactly same as how much you wanted it to be.";
		goto End;
	}
	
	if( $Money != $MoneyDisplay )
	{
		$Log .= "Your current display money: " . $MoneyDisplay . BR . BR;
		
		$Log .= "Your current money does not equal to display money," . BR
			."but let's pretend that we don't care." . BR . BR;
	}
	
	$MoneyWanted = Pack( 'L', $WANTED_MONEY );
	
	$Log .= "Replacing money with: " . $WANTED_MONEY . BR;
	
	$Buffer[ $Pos     ] = $Buffer[ $Pos + 15 ] = $MoneyWanted[ 0 ];
	$Buffer[ $Pos + 1 ] = $Buffer[ $Pos + 16 ] = $MoneyWanted[ 1 ];
	$Buffer[ $Pos + 2 ] = $Buffer[ $Pos + 17 ] = $MoneyWanted[ 2 ];
	$Buffer[ $Pos + 3 ] = $Buffer[ $Pos + 18 ] = $MoneyWanted[ 3 ];
	
	$CalculatedCRC = GTACheckSum( $Buffer );
	
	$Log .= "New calculated checksum: " . $CalculatedCRC . BR;
	
	$Buffer .= Pack( 'L', $CalculatedCRC );
	
	File_Put_Contents( "GTAVCsf1.b", $Buffer );
	
	$Log .= "Success!";
	
	End:
	
	echo $Log;
	
	function GTACheckSum( $Buffer )
	{
		$Length   = StrLen( $Buffer );
		$Checksum = 0;
		
		while( $Length-- > 0 )
		{
			$Checksum += Ord( $Buffer[ $Length ] );
		}
		
		return $Checksum;
	}
	
	function _UnPack( $Format, $Buffer )
	{
		List( , $Buffer ) = UnPack( $Format, $Buffer );
		
		return $Buffer;
	}
